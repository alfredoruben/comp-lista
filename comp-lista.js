{
  const {
    html,
  } = Polymer;
  /**
    `<comp-lista>` Description.

    Example:

    ```html
    <comp-lista></comp-lista>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --comp-lista | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CompLista extends Polymer.Element {

    static get is() {
      return 'comp-lista';
    }

    static get properties() {
      var usu=JSON.parse(sessionStorage.getItem("respuesta")) ;
      return {

        "_firstName": {
          type: String,
          value:usu.firstName
        },
        "_lastName": {
          type: String,
          value:usu.lastName
        },
        "_cuentas": {
          type: Array,
          value: usu.listaCuenta
        },"_idCliente": {
          type: Number,
          value:usu.id
        },"_idCuenta": {
          type: String,
          value:""
        },"_importe": {
          type: Number,
          value:0
        },
        "_descripcion": {
          type: String,
          value:""
        }
      };
    }



    _getMovimientos(e) {
      console.info('comp-lista::_getMovimientos....')
      console.info('idCuenta:'+e.model.item);
      var cuentaJson=JSON.stringify(e.model.item);
      sessionStorage.setItem("cuenta",cuentaJson);
      this.dispatchEvent(new CustomEvent('get-movimientos', {composed: true, bubbles: true}));
    } 
    _openAddMovimiento(e) {
      console.info('comp-lista::_openAddMovimiento');
      var cuentaJson=JSON.stringify(e.model.item);
      sessionStorage.setItem("cuenta",cuentaJson);
      this.dispatchEvent(new CustomEvent('add-movimiento', {composed: true, bubbles: true}));
    }

    _logout(e) {
      console.info('comp-lista::_logout....')
      //this.dispatchEvent(new CustomEvent('logout', {composed: true, bubbles: true}));
    } 

   

    
    static get template() {
      return html `
      <style include="comp-lista-styles comp-lista-shared-styles"></style>
      <slot></slot>
      <cells-component-app-header  text="Hola {{_firstName}}" icon-right="coronita:on" icon-right-label="Logout">
      </cells-component-app-header>
      

    <div style="height: 20px"></div>
    <template is="dom-if" if="{{_cuentas}}" >
      <table class="tabla1">
        <tr>
          <th>ID</th>
          <th>nombre</th>
          <th>numero</th>
          <th>denominacion</th>
          <th>Saldo</th>
          <th>ACCIONES</th>
        </tr>
        <template is="dom-repeat" items="{{_cuentas}}">
          <tr>
            <td>{{item.id}}</td>
            <td>{{item.nombre}}</td>
            <td>{{item.numero}}</td>
            <td>{{item.denominacion}}</td>
            <td>{{item.saldo}}</td>
            <td style="text-align: center;">
              <a on-click="_getMovimientos" title="Ver movimientos" onmouseover="this.style.cursor='pointer';" onmouseout="this.style.cursor='default';">
                <iron-icon icon="coronita:account" style="width: 16px; height: 16px;"></iron-icon>
              </a>
              <a on-click="_openAddMovimiento" title="Realizar una operacion" onmouseover="this.style.cursor='pointer';" onmouseout="this.style.cursor='default';">
                <iron-icon icon="coronita:add" style="width: 16px; height: 16px;"></iron-icon>
              </a>
            </td>
          </tr>
        </template>
      </table>
    </template>
      `;
    }
  }

  customElements.define(CompLista.is, CompLista);
}